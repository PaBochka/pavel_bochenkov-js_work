for (var i = 10; i <= 20; i++) {
	console.log(i * i);
}
var cnt = 0
for (var i = 10; i <= 20; i++) {
	cnt += i;
}
console.log('\n')
console.log(cnt);


function buttononClick() {

	var x1 = document.getElementById('x1').value;
	var x2 = document.getElementById('x2').value;
	var radio_sum = document.getElementById('radio_sum')
	var radio_mul = document.getElementById('radio_mul')
	if (x1.length == 0 || x2.length == 0){
		alert('NOT INPUT!');
	} 
	else {
		x1 = parseInt(x1);
		x2 = parseInt(x2);
	
		var resultDiv = document.getElementById('result');
		resultDiv.innerHTML = "";
		var prime_numbers = document.getElementById('numbers');
		prime_numbers.innerHTML = "";
		if (Number.isNaN(x1) || Number.isNaN(x2)){
			alert('input number value.');
		} else {
			if (radio_sum.checked){
				var count = 0
				for (var i = x1; i <= x2; i++){
					count += i;
				} 
				resultDiv.append(" sum " + count);
			}
			if (radio_mul.checked){
				var count = 1
				for (var i = x1; i <= x2; i++){
					count *= i;
				} 
				resultDiv.append(" mul " + count);
			}
			beg = x1;
			end = x2;
			if(beg == 1) beg += 1;
			nextStep:
				for (var i = beg; i <= end; i++){
					for (var j = 2; j < i; j++){
						if ((i % j) == 0) continue nextStep;
					}
					prime_numbers.append(i + "\n");
				}
		}
	}
}

function clearInput() {
	document.getElementById('x1').value = "";
	document.getElementById('x2').value = "";
}